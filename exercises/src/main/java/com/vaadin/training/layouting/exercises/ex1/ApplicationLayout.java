package com.vaadin.training.layouting.exercises.ex1;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import com.vaadin.training.layouting.exercises.MainLayout;

import java.io.Serial;

@Route(value = ApplicationLayout.ROUTE, layout = MainLayout.class)
@RouteAlias(value = "", layout = MainLayout.class)
public class ApplicationLayout extends VerticalLayout {

	@Serial
	private static final long serialVersionUID = 1L;

	public static final String ROUTE = "ex1";
	public static final String TITLE = "Exercise 1";

	public ApplicationLayout() {
		setSizeFull();
		setPadding(false);
		setSpacing(false);

		final Div header = new Div();
		header.setText("This is the header. My height is 150 pixels");
		header.setClassName("header");

		final Div navigation = new Div();
		navigation.setClassName("navigation");

		final FlexLayout content = new FlexLayout();
		content.setClassName("content");
		content.setFlexWrap(FlexLayout.FlexWrap.WRAP);
		content.getStyle().set("overflow", "auto");

		final Div footer = new Div();
		footer.setText("This is the footer area. My height is 100 pixels");
		footer.setClassName("footer");

		Button btnAdd = new Button("Add");
		navigation.add(btnAdd);
		navigation.setWidth("25%");
		navigation.setHeightFull();
		content.setSizeFull();
		content.setHeightFull();

		btnAdd.addClickListener((ComponentEventListener<ClickEvent<Button>>) event -> {
			content.add(createBlock());
		});

		HorizontalLayout hori1 = new HorizontalLayout();
		hori1.setSizeFull();
		hori1.setSpacing(false);
		hori1.add(navigation, content);

		header.setHeight("150px");
		header.setWidthFull();

		footer.setHeight("100px");
		footer.setWidthFull();

		add(header, hori1, footer);
//		add(header, navigation, content, footer);
	}

	/**
	 * Ignore this method for now.
	 *
	 * @return
	 */
	private Div createBlock() {
		final Div button = new Div();
		button.setText("Block");
		button.getStyle().set("background", "white");
		button.setHeight("100px");
		button.setWidth("100px");
		button.getStyle().set("margin", "2px");
		return button;
	}

}
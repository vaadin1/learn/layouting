package com.vaadin.training.layouting.exercises.ex2;

import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.dom.ElementFactory;
import com.vaadin.flow.router.Route;
import com.vaadin.training.layouting.exercises.MainLayout;

@Route(value = UseFormLayout.ROUTE, layout = MainLayout.class)
public class UseFormLayout extends VerticalLayout {
    public static final String ROUTE = "ex2";
    public static final String TITLE = "Exercise 2";

    public UseFormLayout(){
        setSizeFull();

        FormLayout form = new FormLayout();
        form.setSizeFull();

        TextField txtFirstName = new TextField();
        txtFirstName.setWidthFull();
        form.addFormItem(txtFirstName, "First Name");
        TextField txtLastName = new TextField();
        txtLastName.setWidthFull();
        form.addFormItem(txtLastName, "Last Name");
        TextField txtEmail = new TextField();
        txtEmail.setWidthFull();
        FormLayout.FormItem itemEmail = form.addFormItem(txtEmail, "Email");
        itemEmail.getElement().setAttribute("colspan", "2");

        FlexLayout flexPhone = new FlexLayout();
        NumberField txtPhone = new NumberField();
        Checkbox chkPhone = new Checkbox("Do not call");
        flexPhone.add(txtPhone, chkPhone);
        flexPhone.setAlignItems(Alignment.END);
        flexPhone.setWidthFull();
        flexPhone.expand(txtPhone);
        FormLayout.FormItem itemPhone = form.addFormItem(flexPhone, "Phone");
        itemPhone.getElement().setAttribute("colspan", "2");

        PasswordField fieldPass = new PasswordField();
        fieldPass.setWidthFull();
        form.addFormItem(fieldPass, "Password");

        form.getElement().appendChild(ElementFactory.createBr());

        PasswordField fieldRepeatPass = new PasswordField();
        fieldRepeatPass.setWidthFull();
        form.addFormItem(fieldRepeatPass, "RepeatPassword");

        add(form);
    }
}
